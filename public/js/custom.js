$( function() {
    $( "#datepicker-input-1" ).datepicker({
        minDate: 0,
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true
    });
    
    $( "#datepicker-input-1" ).change(function() {
        var from = $( "#datepicker-input-1" ).val().split("-");
        var moded = new Date(from[2], from[1] - 1, from[0]);
        moded.setDate(moded.getDate() + 1);
        $( "#datepicker-input-2" ).datepicker({
            minDate: moded,
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });
    });
  });