## About Leave Management System

This is a Laravel based simple Leave Managemnt Software, where super admin can add Employees and Other Admins. Each Employee can add make their leave request through the system and all admins can view the requests.

Please follow the below steps and commands to run the project locally.

## Setup instructions:

- git clone https://gitlab.com/vipinpg/leave-management-system.git
- copy .env.example .env
- php composer.phar update
- php artisan key:generate
- npm install
- npm install --global cross-env

## Database setup:

- Create database leavemngt_db (utf8_unicode_ci)
- php artisan migrate
- php artisan db:seed
