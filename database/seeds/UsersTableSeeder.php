<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$users = [
			['Admin', 'vipin@pgee.in', 'admin123', 1, 1, 0, 0, '']
		];

		foreach ($users as $user)
		{
			DB::table('users')->insert([
				'name' 			=> $user[0],
				'email' 		=> $user[1],
				'password' 		=> sha1($user[2]),
				'is_root' 		=> $user[3],
				'is_admin' 		=> $user[4],
				'is_suspended' 	=> $user[5],
				'is_locked' 	=> $user[6],
				'lock_reason' 	=> $user[7],
				'created_at'	=> new DateTime(),
				'updated_at'	=> new DateTime(),
			]);
		}
	}
}
