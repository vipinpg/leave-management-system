<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->boolean('is_root')->default(0);
            $table->boolean('is_admin')->default(0);
            $table->boolean('is_suspended')->default(0);
			$table->boolean('is_locked')->default(0);
			$table->string('lock_reason')->default('');
			$table->timestamp('lock_time')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
