<?php

return [
	'site' => [
		'title' 		=> 'Leave Management System',
		'full-title' 	=> 'Leave Management System',
		'desc' 			=> ''
	],
	'pagination' => [
		'page-size' 		=> 10,
		'blog-items' 		=> 3,
		'blog-items-more' 	=> 3,
		'search-items' 		=> 9
	]

];