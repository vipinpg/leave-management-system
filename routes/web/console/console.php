<?php

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
*/

Route::group(['namespace' => 'Console'], function()
{
    Route::get('', 'DashboardController@index');

    // Users
    Route::group(['prefix' => 'users'], function()
    {
        Route::get('', 'UserController@index');
        Route::get('add', 'UserController@add');
        Route::post('add', 'UserController@add');
        Route::get('{user}/edit', 'UserController@edit');
        Route::post('{user}/edit', 'UserController@edit');
        Route::get('{user}/delete', 'UserController@delete'); 
        Route::get('admins', 'UserController@admins');
        Route::get('employees', 'UserController@employees');
        Route::get('change-password', 'UserController@changepassword');
        Route::post('change-password', 'UserController@changepassword'); 
    });

    // Leaves
    Route::group(['prefix' => 'leaves'], function()
    {
        Route::get('', 'LeavesController@index');
        Route::get('my-leaves', 'LeavesController@myLeaves');
        Route::get('add', 'LeavesController@add');
        Route::post('add', 'LeavesController@add');
        Route::get('{leave}/delete', 'LeavesController@delete');    
    });

});
