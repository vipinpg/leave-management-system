<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function() {

	/*
	|--------------------------------------------------------------------------
	| Login
	|--------------------------------------------------------------------------
	*/
	Route::group(['prefix' => 'console'], function()
	{
        Route::group(['namespace' => 'Core\Authentication'], function()
	    {
            Route::get('login', 'AuthenticationController@login');
            Route::post('login', 'AuthenticationController@login');
            Route::get('logout', 'AuthenticationController@logout');
        });
		
		Route::group(['middleware' => ['web', 'user']], function()
		{
			@include 'web/console/console.php';
		});
	});
	@include "web/common/common.php";
  
});