<?php

namespace App\Models\Core;
use App\Models\AppModel;
use Illuminate\Database\Eloquent\Model;

class Login extends AppModel
{
	/*
	|--------------------------------------------------------------------------
	| Model configuration
	|--------------------------------------------------------------------------
	*/
    protected $table = 'user_logins';

    /*
    |--------------------------------------------------------------------------
    | Users
    |--------------------------------------------------------------------------
    */
    public function user()
    {
		  return $this->belongsTo('App\Models\Core\Users');
    }
}
