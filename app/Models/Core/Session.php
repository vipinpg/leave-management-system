<?php

namespace App\Models\Core;

use App\Models\AppModel;

class Session extends AppModel
{
	/*
	|--------------------------------------------------------------------------
	| Model configuration
	|--------------------------------------------------------------------------
	*/
    protected $table 		= 'console_sessions';
    protected $auditEnabled = false;
    public $timestamps 		= false;

    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    */
    public function user()
    {
		return $this->belongsTo('App\Models\Core\Users');
    }
}
