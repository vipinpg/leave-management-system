<?php

namespace App\Models\Core;
use App\Models\AppModel;
use Illuminate\Database\Eloquent\Collection;

use Illuminate\Database\Eloquent\Model;

class User extends AppModel
{

	/*
	|--------------------------------------------------------------------------
	| Model configuration
	|--------------------------------------------------------------------------
	*/
	protected $table 	= 'users';
	public $dates 		= ['lock_time', 'last_login'];

	/*
	|--------------------------------------------------------------------------
	| Full name
	|--------------------------------------------------------------------------
	*/
	public function name()
	{
		return "{$this->name}";
	}

}
