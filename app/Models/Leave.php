<?php

namespace App\Models;
use App\Models\AppModel;
use Illuminate\Database\Eloquent\Collection;

use Illuminate\Database\Eloquent\Model;

class Leave extends AppModel
{

	/*
	|--------------------------------------------------------------------------
	| Model configuration
	|--------------------------------------------------------------------------
	*/
	protected $table 	= 'leaves';
	public $dates 		= ['from_date','to_date'];


}
