<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppModel extends Model
{
	/*
	|--------------------------------------------------------------------------
	| Override getAttribute to support camelCase
	|--------------------------------------------------------------------------
	*/
	public function getAttribute($key)
	{
		return parent::getAttribute(snake_case($key));
	}

	/*
	|--------------------------------------------------------------------------
	| Override setAttribute to support camelCase
	|--------------------------------------------------------------------------
	*/
	public function setAttribute($key, $value)
	{
		return parent::setAttribute(snake_case($key), $value);
	}
}