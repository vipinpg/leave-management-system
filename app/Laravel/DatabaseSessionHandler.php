<?php

namespace App\Laravel;

use App\Core\Core;

class DatabaseSessionHandler extends \Illuminate\Session\DatabaseSessionHandler
{
    /*
    |--------------------------------------------------------------------------
    | Logged in user ID
    |--------------------------------------------------------------------------
    */
    public function userId()
    {
        $user = Core::user();

        return $user ? $user->id : null;
    }
}