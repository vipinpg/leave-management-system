<?php

namespace App\Core;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Session;

class CoreGuard
{
	/*
	|--------------------------------------------------------------------------
	| Properties
	|--------------------------------------------------------------------------
	*/
	private $user 			= null;
	private $userId 		= null;

	/*
	|--------------------------------------------------------------------------
	| Get properties
	|--------------------------------------------------------------------------
	*/
	public function user() { return $this->user; }
	public function userId() { return $this->userId; }

	/*
	|--------------------------------------------------------------------------
	| Controller whitelist
	|--------------------------------------------------------------------------
	*/
	private $controllerWhitelist = [
		// Core
		'Core\Authentication\AuthenticationController',
		'Console\DashboardController',
		'Console\LeavesController',
		'Console\UserController',
];

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/
	public function __construct()
	{
		// Initialize
		$this->user = Session::get('session.user');

		if ($this->user !== null) {
			// Initialize employee ID
			$this->userId = $this->user->id;
		}
	}

	/*
	|--------------------------------------------------------------------------
	| Check request authorization
	|--------------------------------------------------------------------------
	*/
	public function isAuthorized()
	{

		// Get controller and action
		$route = request()->route()->getAction()['controller'];

		list($controller, $action) = explode('@', $route);

		$controller = preg_replace('/^' . preg_quote('App\Http\Controllers\\', '/') . '/', '', $controller);

		// Check if the controller is white listed
		if (in_array($controller, $this->controllerWhitelist))
		{
			return true;
		}

		// Check controller and action access


		// Return false for everyone else
		return false;
	}
}