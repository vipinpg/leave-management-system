<?php

namespace App\Core;

class Core
{
    /*
    |--------------------------------------------------------------------------
    | Core objects
    |--------------------------------------------------------------------------
    */
    private static $session 	= null;
    private static $user 	    = null;
    private static $guard 		= null;
	private static $storage 	= null;
  
    /*
    |--------------------------------------------------------------------------
    | Initialize App
    |--------------------------------------------------------------------------
    */
    public static function initialize()
    {
        self::$session 	= new CoreSession();
        self::$user     = new CoreUser();
        self::$guard 	= new CoreGuard();
        self::$storage 	= new CoreStorage();

    }

    /*
    |--------------------------------------------------------------------------
    | Get core objects
    |--------------------------------------------------------------------------
    */
    public static function session() { return self::$session; }
    public static function user() { return self::$user; }
    public static function guard() { return self::$guard; }
    public static function storage() { return self::$storage; }
}