<?php

namespace App\Core;

class CoreStorage
{
	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/
	private $basePath = '';

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/
	public function __construct()
	{
		// Build base path
		$this->basePath = public_path() . DIRECTORY_SEPARATOR . "storage";

		// Make sure path exists
		@mkdir($this->basePath);
	}

	/*
	|--------------------------------------------------------------------------
	| Base path
	|--------------------------------------------------------------------------
	*/
	public function basePath()
	{
		return $this->basePath;
	}

	/*
	|--------------------------------------------------------------------------
	| Assert path
	|--------------------------------------------------------------------------
	*/
	public function assert($path = '')
	{
		$path = ($path[0] == '/') ? substr($path, 1, strlen($path) - 1) : $path;

		if (!is_dir($this->basePath . DIRECTORY_SEPARATOR . $path))
		{
			$pathArray 	= explode('/', $path);
			$realPath 	= $this->basePath;

			if (!empty($pathArray))
			{
				foreach ($pathArray as $pathValue)
				{
					if (trim($pathValue) !== '')
					{
						$realPath .= DIRECTORY_SEPARATOR . $pathValue;

						if (!is_dir($realPath))
						{
							@mkdir($realPath);
						}
					}
				}
			}
		}
	}

	/*
	|--------------------------------------------------------------------------
	| Get unique filename
	|--------------------------------------------------------------------------
	*/
	public function getUniqueFilename($extension = '')
	{
		return time() . "_" . md5(uniqid(time())) . "." . $extension;
	}
}