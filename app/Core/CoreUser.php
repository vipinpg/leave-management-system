<?php

namespace App\Core;

use Illuminate\Support\Facades\View;

class CoreUser
{
	/*
	|--------------------------------------------------------------------------
	| User
	|--------------------------------------------------------------------------
	*/
	private $user = null;

	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/
	public function __construct()
	{
		$this->user = Core::session()->user();

		View::share('coreUser', $this->user);
	}

	/*
	|--------------------------------------------------------------------------
	| Get raw user object
	|--------------------------------------------------------------------------
	*/
	public function raw()
	{
		return $this->user;
	}

	/*
	|--------------------------------------------------------------------------
	| Check if user is logged in
	|--------------------------------------------------------------------------
	*/
	public function isLoggedIn()
	{
		return ($this->user !== null);
	}

	/*
	|--------------------------------------------------------------------------
	| Check if user is root
	|--------------------------------------------------------------------------
	*/
	public function isRoot()
	{
		if ($this->isLoggedIn())
		{
			return ($this->user->isRoot == 1);
		}

		return false;
	}

	/*
	|--------------------------------------------------------------------------
	| Check user role
	|--------------------------------------------------------------------------
	*/
	public function isAdmin()
	{
		if ($this->isLoggedIn())
		{
			return ($this->user->isAdmin == 1);
		}

		return false;
	}

	/*
	|--------------------------------------------------------------------------
	| Get user properties
	|--------------------------------------------------------------------------
	*/
	public function __get($key)
	{
		if (isset($this->user->$key))
		{
			return $this->user->$key;
		}

		return null;
	}

	/*
	|--------------------------------------------------------------------------
	| Call user methods
	|--------------------------------------------------------------------------
	*/
	public function __call($key, $arguments)
	{
		if (method_exists($this->user, $key))
		{
			return $this->user->$key(...$arguments);
		}

		if ($this->__get($key))
		{
			return $this->__get($key);
		}

		return null;
	}
}