<?php

namespace App\Http\Controllers\Core\Authentication;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Core\Core;
use App\Http\Controllers\Controller;
use App\Models\Core\Login;
use App\Models\Core\User;

class AuthenticationController extends Controller
{
    /*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/
	public function __construct()
	{
		$this->setTitle('Login', config('leave.site.full-title'), '', 'login');
		$this->setViewPath('console.authentication');
        $this->setActivePath('console.authentication');
	}
    /*
    |--------------------------------------------------------------------------
    | Login
    |--------------------------------------------------------------------------
    */
    public function login(Request $request)
    {
        $maxFailedAttempts 			= 4;
        $maxFailedAttemptsWindow 	= 2; // In minutes

        if ($request->isMethod('post'))
        {
            
            $username 	= $request->input('username');
            $password 	= $request->input('password');
            
            // Check if username exists in database
            $user	= User::where('email', $username)->first();
            
            // Initialize
            $result 	= false;
            $reason 	= '';
            $redirect 	= 'login';

            if ($user)
            {
                // Check if employee account is locked
                // In case account is locked, check if we can unlock it
                if ($user->isLocked)
                {
                    $unlockTime = $user->lockTime ? $user->lockTime->addMinutes(15) : null;

                    if (Carbon::now() > $unlockTime)
                    {
                        // Unlock employee
						$user->isLocked 	= 0;
						$user->lockReason 	= '';
						$user->lockTime 	= null;

						$user->save();
                    }
                    else
                    {
                        $reason = 'Account is locked';
                    }
                }

                // Check if employee account is suspended
                if ($user->isSupended)
                {
                    $reason  = 'Account is suspended';
                }

                // Authenticate
				// if (Adldap::getDefaultProvider()->auth()->attempt($username, $password))
				if ($user->password == sha1($password))
                {
                    // Set as logged in
                    $result = true;

                    if ($user->isLocked)
                    {
                        $this->setErrorMessage('Due to security reasons, your account has been locked.<br/>For more information, please contact the IT department.');
                    }
                    elseif ($user->isSuspended)
                    {
                        $this->setErrorMessage('Your account has been suspended by management.<br/>For more information, please contact the HR department.');
                    }
                    else
                    {
                        // Update the last login
						$user->lastLogin = Carbon::now()->toDateTimeString();
						$user->save();
                        // Create session
                        Core::session()->create($user);

                        // Redirect to dashboard
                        $redirect = 'dashboard';
                    }
                }
                else
                {
                    // Authentication failed
                    if (!$user->isLocked && !$user->isSuspended)
                    {
                        $reason = 'Invalid username and/or password';
                        // Get failed login attempts
                        $failedLogins = Login::where('user_id', '=', $user->id)
                            ->where('result', '=', 0)
                            ->where('created_at', '>', ($user->lastLogin ? $user->lastLogin->toDateTimeString() : ''))
                            ->where('created_at', '>', Carbon::now()->subMinutes($maxFailedAttemptsWindow)->toDateTimeString())
                            ->count();

                        // Check if we need to lock the employee account
                        if ($failedLogins >= ($maxFailedAttempts - 1))
                        {
							$user->isLocked 	= 1;
							$user->lockTime 	= Carbon::now()->toDateTimeString();
							$user->lockReason 	= 'Too many failed login attempts.';

							$user->save();
                        }
                    }
                }
            }

            if (!$result)
            {
                $reason = 'Invalid username and/or password';
                $this->setErrorMessage('Invalid username and/or password.');
            }

            // Register login attempt
            $attempt 				= new Login();
            $attempt->username 		= $username;
            $attempt->userId 	    = $user ? $user->id : null;
            $attempt->ipAddress 	= $request->ip();
            $attempt->userAgent 	= $request->header('User-Agent');
            $attempt->result 		= $result;
            $attempt->reason 		= $reason;
            $attempt->save();

            // Redirect
            if ($redirect == 'dashboard')
            {
                return redirect('/console');
            }
            else
            {
                return redirect('/console/login')->withInput(['username' => $username]);
            }
            
        }

        return $this->view('login', compact(
            ''
        ));
    }

    /*
    |--------------------------------------------------------------------------
    | Logout
    |--------------------------------------------------------------------------
    */
    public function logout()
    {
        Core::session()->destroy();

        $this->setSuccessMessage('You have been successfully logged out.');

        return redirect('/console/login');
    }
}