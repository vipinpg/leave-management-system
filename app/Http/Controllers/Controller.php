<?php

namespace App\Http\Controllers;

use App\Core\Core;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/*
	|--------------------------------------------------------------------------
	| Base view
	|--------------------------------------------------------------------------
	*/
    private $baseView = '';

	/*
	|--------------------------------------------------------------------------
	| Breadcrumb
	|--------------------------------------------------------------------------
	*/
    private $breadcrumb = [];

	/*
	|--------------------------------------------------------------------------
	| Active path
	|--------------------------------------------------------------------------
	*/
    private $activePath = [];

    /*
    |--------------------------------------------------------------------------
    | Set page title
    |--------------------------------------------------------------------------
    */
    public function setTitle($title = '', $subtitle = '', $alignment = '', $identifier = '')
    {
    	// Title
        View::share('pageTitle', $title);

        // Sub-title
        if ($subtitle)
		{
			View::share("pageSubTitle", $subtitle);
		}

		// Alignment
		if ($alignment)
		{
			$class = 'text-' . $alignment;
			View::share('pageTitleAlignment', $class);
		}

		// Identifier
		if ($identifier)
		{
			View::share('pageIdentifier', $identifier);
		}
    }

    /*
    |--------------------------------------------------------------------------
    | Set active path
    |--------------------------------------------------------------------------
    */
    public function setActivePath($path = null)
    {
    	if (!is_array($path))
		{
			$pathArray = explode('.', $path);
			$path = [];
			$fullPath = '';

			foreach ($pathArray as $pathItem)
			{
				$fullPath .= (!empty($path) ? '.' : '') . $pathItem;
				$path[] = $fullPath;
			}
		}

    	$this->activePath = $path ? $path : [];

		View::share('activePath', $this->activePath);
    }

	/*
	|--------------------------------------------------------------------------
	| Add active path
	|--------------------------------------------------------------------------
	*/
    public function addActivePath($path = '')
	{
		$this->activePath[] = $path;

		View::share('activePath', $this->activePath);
	}

	/*
	|--------------------------------------------------------------------------
	| Set breadcrumb
	|--------------------------------------------------------------------------
	*/
    public function setBreadcrumb($bc = [])
	{
		$this->breadcrumb = $bc;
		View::share('breadcrumb', $this->breadcrumb);
	}

	/*
	|--------------------------------------------------------------------------
	| Add breadcrumb
	|--------------------------------------------------------------------------
	*/
	public function addBreadcrumb($title = '', $link = null)
	{
		$this->breadcrumb[$title] = $link;
		View::share('breadcrumb', $this->breadcrumb);
	}

	/*
	|--------------------------------------------------------------------------
	| Set view path
	|--------------------------------------------------------------------------
	*/
	public function setViewPath($baseView = '')
	{
		$this->baseView = $baseView;
	}

	/*
	|--------------------------------------------------------------------------
	| Get view
	|--------------------------------------------------------------------------
	*/
	public function view($view = '', $vars = [])
	{
		return view($this->baseView . '.' . $view, $vars);
	}

    /*
    |--------------------------------------------------------------------------
    | Set flash message
    |--------------------------------------------------------------------------
    */
    public function setFlash($title = '', $message = '', $type = '')
    {
        $title = !empty($title)
            ? '<strong>' . strtoupper($title) . '</strong><br/>'
            : $title;

        flash($title . $message, $type)->important();
    }

    /*
    |--------------------------------------------------------------------------
    | Flash messages shortcuts
    |--------------------------------------------------------------------------
    */
    public function setSuccessMessage($message = '') { $this->setFlash('Success', $message, 'success'); }
    public function setErrorMessage($message = '') { $this->setFlash('Error', $message, 'danger'); }
    public function setWarningMessage($message = '') { $this->setFlash('Warning', $message, 'warning'); }
    public function setInfoMessage($message = '') { $this->setFlash('Heads up', $message, 'info'); }

}