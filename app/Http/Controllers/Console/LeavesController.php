<?php

namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Core\Core;
use App\Http\Controllers\Controller;
use App\Models\Leave;
use App\Models\Core\User;

class LeavesController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/
	public function __construct()
	{
		$this->setTitle('Leaves', config('leave.site.full-title'), '', 'leaves');
		$this->setViewPath('console.leaves');
		$this->setActivePath('console.leaves');
		$this->setBreadcrumb([
			'Dashboard' => null,
			'Leaves' => null
		]);
	}

	/*
	|--------------------------------------------------------------------------
	| Index
	|--------------------------------------------------------------------------
	*/	
	public function index()
	{
		if(!Core::user()->isAdmin()){
			return redirect("/console/leaves/my-leaves");
		}

		$leaves = Leave::leftJoin('users', 'leaves.user_id', '=', 'users.id')
						->selectRaw('leaves.*, users.name as user_name')
						->where('leaves.deleted_at', null)
						->where('users.deleted_at', null)
						->orderBy('leaves.id', 'desc')
						->paginate(config('leave.pagination.page-size'));							

		return $this->view('index', compact(
				'leaves'
		));
	}

	/*
	|--------------------------------------------------------------------------
	| My Leaves - Employees individual leave list
	|--------------------------------------------------------------------------
	*/	
	public function myLeaves()
	{
		if(Core::user()->isAdmin()){
			return redirect("/console/leaves");
		}

		$leaves = Leave::leftJoin('users', 'leaves.backup_user_id', '=', 'users.id')
						->selectRaw('leaves.*, users.name as backup_user_name')
						->where('leaves.deleted_at', null)
						->where('leaves.user_id', '=', Core::user()->id)
						->orderBy('leaves.id', 'desc')
						->paginate(config('leave.pagination.page-size'));			

		return $this->view('my-leaves', compact(
				'leaves'
		));
	}

	/*
	|--------------------------------------------------------------------------
	| Add
	|--------------------------------------------------------------------------
	*/	
	public function add(Request $request)
	{ 
		if(Core::user()->isAdmin()){
			return redirect("/console/leaves");
		}
		$this->setTitle('Add Leave', config('leave.site.full-title'), '', 'add-leave');
		$users = user::where('deleted_at', null)
						->where('id', '!=', Core::user()->id)
						->where('is_admin', '=', 0)
						->get(); 
		if ($request->isMethod('post'))
		{
			$this->validate(
				$request,
				[
					'from_date' 			=> 'required',
					'to_date' 				=> 'required',
					'backup_user_id' 		=> 'required',
					'reason' 				=> 'required',
				],
				[
					'backup_user_id.required' => 'The backup user field is required.'
				]
			);


			$leave 					= new Leave();
			$leave->userId 			= Core::user()->id;
			$leave->fromDate 		= Carbon::createFromFormat('d-m-Y', $request->input('from_date'));
			$leave->toDate 			= Carbon::createFromFormat('d-m-Y', $request->input('to_date'));
			$leave->backupUserId 	= $request->input('backup_user_id');
			$leave->reason 			= $request->input('reason');
			
			$leave->save();

			$this->setSuccessMessage('Leave request has been successfully created.');
			return redirect("/console/leaves");
		}

		return $this->view('edit', compact(
			'users'
		));
	}

	/*
	|--------------------------------------------------------------------------
	| Delete
	|--------------------------------------------------------------------------
	*/
	public function delete(Leave $leave)
	{
		$leave = Leave::where('id', '=', $leave->id)->first();
		$leave->deletedAt 	= Carbon::now();
		//$leave->save();
		$this->setSuccessMessage('Leave has been successfully deleted.');
		return redirect('/console/leaves');
	}

}