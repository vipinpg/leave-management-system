<?php

namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Core\Core;
use App\Http\Controllers\Controller;
use App\Models\Core\Login;
use App\Models\Core\User;


class UserController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/
	public function __construct()
	{

		$this->setTitle('Change Password', config('leave.site.full-title'), '', 'change-password');
		$this->setViewPath('console.user');
		$this->setActivePath('console.user');
		
		$this->setBreadcrumb([
			'Dashboard' => null,
			'User' => null
		]);
	}

	/*
	|--------------------------------------------------------------------------
	| Index
	|--------------------------------------------------------------------------
	*/	
	public function index()
	{
		if(!Core::user()->isRoot()){
			return redirect("/console");
		}

		$users = User::where('deleted_at', null)
						->where('is_root', '=', 0)
						->orderBy('id', 'desc')
						->paginate(config('leave.pagination.page-size'));
		return $this->view('index', compact(
				'users'
		));
	}

	/*
	|--------------------------------------------------------------------------
	| Admins
	|--------------------------------------------------------------------------
	*/	
	public function admins()
	{
		if(!Core::user()->isRoot()){
			return redirect("/console");
		}

		$users = User::where('deleted_at', null)
						->where('is_root', '=', 0)
						->where('is_admin', '=', 1)
						->orderBy('id', 'desc')
						->paginate(config('leave.pagination.page-size'));
		return $this->view('index', compact(
				'users'
		));
	}

	/*
	|--------------------------------------------------------------------------
	| Employees
	|--------------------------------------------------------------------------
	*/	
	public function employees()
	{
		if(!Core::user()->isAdmin()){
			return redirect("/console");
		}

		$users = User::where('deleted_at', null)
						->where('is_root', '=', 0)
						->where('is_admin', '=', 0)
						->orderBy('id', 'desc')
						->paginate(config('leave.pagination.page-size'));
		return $this->view('index', compact(
				'users'
		));
	}	

	/*
	|--------------------------------------------------------------------------
	| Add
	|--------------------------------------------------------------------------
	*/	
	public function add(Request $request)
	{
		if(!Core::user()->isRoot()){
			return redirect("/console");
		}

		$this->setTitle('Add User', config('leave.site.full-title'), '', 'add-user');
		if ($request->isMethod('post'))
		{
			$this->validate(
				$request,
				[
					'name' 		=> 'required',
					'email'		=> 'required|email|unique:users,email',
					'password'	=> 'required|min:6|max:16',
					'is_admin'	=> 'required',
				],
				[
					'is_admin.required' => 'The user type field is required.'
				]
			);

			$user 				= new User();
			$user->name 		= $request->input('name');
			$user->email 		= $request->input('email');
			$user->password 	= sha1($request->input('password'));
			$user->isAdmin 		= $request->input('is_admin');

			$user->save();

			$this->setSuccessMessage('User has been successfully created.');
			return redirect("/console/users/{$user->id}/edit");
		}
		return $this->view('edit');
	}

	/*
	|--------------------------------------------------------------------------
	| Edit
	|--------------------------------------------------------------------------
	*/
	public function edit(Request $request, User $user)
	{
		if(!Core::user()->isRoot()){
			return redirect("/console");
		}

		$this->setTitle('Edit User', config('leave.site.full-title'), '', 'edit-user');
		$user = User::where('id', '=', $user->id)->first();

		if ($request->isMethod('post'))
		{
			$this->validate(
				$request,
				[
					'name' 		=> 'required',
					'is_admin'	=> 'required',
				],
				[
					'is_admin.required' => 'The user type field is required.'
				]
			);

			$user->name 		= $request->input('name');
			$user->isAdmin 		= $request->input('is_admin');

			$user->save();
			$this->setSuccessMessage('User has been successfully updated.');
			return redirect("/console/users/{$user->id}/edit");
		}
		return $this->view('edit', compact(
			'user'
		));

	}

	/*
	|--------------------------------------------------------------------------
	| Delete
	|--------------------------------------------------------------------------
	*/
	public function delete(User $user)
	{
		if(!Core::user()->isRoot()){
			return redirect("/console");
		}

		$user = User::where('id', '=', $user->id)->first();
		$user->deletedAt 	= Carbon::now();
		$user->save();
		$this->setSuccessMessage('User has been successfully deleted.');
		return redirect('/console/users');
	}

	/*
	|--------------------------------------------------------------------------
	| Change Password
	|--------------------------------------------------------------------------
	*/	
	public function changePassword(Request $request, User $user)
	{
		$this->setActivePath('console.user.changepassword');
		
		if ($request->isMethod('post'))
		{
			$this->validate(
				$request,
				[
					'current_password' 	=> 'required|min:6|max:16',
					'new_password' 		=> 'required|confirmed|min:6|max:16'
				],
				[]
			);
			$user = User::where('id', '=', Core::user()->id())->first();
			if(sha1($request->input('current_password')) == $user->password){
				$user->password 	= sha1($request->input('new_password'));
				$user->save();
				$this->setSuccessMessage('Password changed successfully.');
			}
			else{
				$this->setErrorMessage('Your current password is wrong.');
			}
			return redirect('/console/users/change-password');
		}
		return $this->view('change-password');
	}
}