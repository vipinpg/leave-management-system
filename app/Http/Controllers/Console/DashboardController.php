<?php

namespace App\Http\Controllers\Console;

use Illuminate\Http\Request;

use App\Core\Core;
use App\Http\Controllers\Controller;
use App\Models\Core\Login;
use App\Models\Core\User;

class DashboardController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Constructor
	|--------------------------------------------------------------------------
	*/
	public function __construct()
	{
		$this->setTitle('Dashboard', config('leave.site.full-title'), '', 'dashboard');
		$this->setViewPath('console.dashboard');
		$this->setActivePath('console.dashboard');
		$this->setBreadcrumb([
			'Dashboard' => null
		]);
	}

	/*
	|--------------------------------------------------------------------------
	| Index
	|--------------------------------------------------------------------------
	*/	
	public function index()
	{
		return $this->view('index');
	}
}