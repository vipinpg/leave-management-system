<?php

namespace App\Http\Middleware;

use Closure;

use App\Core\Core;

class InitializeCore
{
    /*
    |--------------------------------------------------------------------------
    | Initialize App
    |--------------------------------------------------------------------------
    */
    public function handle($request, Closure $next)
    {
        Core::initialize();

        return $next($request);
    }
}
