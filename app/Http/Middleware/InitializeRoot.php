<?php

namespace App\Http\Middleware;

use Closure;

use App\Core\Core;

class InitializeRoot
{
    /*
    |--------------------------------------------------------------------------
    | Initialize root
    |--------------------------------------------------------------------------
    */
    public function handle($request, Closure $next)
    {
        if (Core::employee()->isRoot() !== true)
        {
            return redirect('/console');
        }

        return $next($request);
    }
}
