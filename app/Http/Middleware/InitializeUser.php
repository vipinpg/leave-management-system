<?php

namespace App\Http\Middleware;

use Closure;

use App\Core\Core;

class InitializeUser
{
    /*
    |--------------------------------------------------------------------------
    | Initialize App
    |--------------------------------------------------------------------------
    */
    public function handle($request, Closure $next)
    {
        if (!Core::user()->isLoggedIn())
        {
            return redirect('/console/login');
        }

		if (!Core::guard()->isAuthorized())
		{
			return redirect('/console');
		}

        return $next($request);
    }
}
