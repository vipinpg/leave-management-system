<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Query\Builder;

use Carbon\Carbon;

class EloquentServiceProvider extends ServiceProvider
{
	public function boot()
	{
		Builder::macro('expired', function()
		{
			return $this->where($this->from . '.expiry_date', '<', Carbon::today()->startOfDay());
		});

		Builder::macro('notExpired', function()
		{
			return $this->where($this->from . '.expiry_date', '>=', Carbon::today()->startOfDay());
		});
	}
}