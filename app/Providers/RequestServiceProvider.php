<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Request;

use Carbon\Carbon;

class RequestServiceProvider extends ServiceProvider
{
	public function boot()
	{
		Request::macro('toCarbon', function($input = '', $format = 'd-m-Y')
		{
			if (empty($input))
			{
				return null;
			}

			$carbon = null;

			try
			{
				$carbon = Carbon::createFromFormat($format, $this->input($input));
			}
			catch (\Exception $exception){}

			return $carbon;
		});

		Request::macro('toSqlDate', function($input = '', $format = 'd-m-Y')
		{
			if (empty($input))
			{
				return null;
			}

			$carbon = null;

			try
			{
				$carbon = Carbon::createFromFormat($format, $this->input($input));
			}
			catch (\Exception $exception){}

			return $carbon ? $carbon->format('Y-m-d') : null;
		});
	}
}