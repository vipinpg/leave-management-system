<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\ServiceProvider;

class CollectionServiceProvider extends ServiceProvider
{
	public function boot()
	{
		Collection::macro('prependEmpty', function()
		{
			return collect(['' => ''] + $this->all());
		});
	}
}