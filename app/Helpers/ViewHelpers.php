<?php

/*
|--------------------------------------------------------------------------
| Get day
|--------------------------------------------------------------------------
*/
function activePath($path = '', $activePath = [], $true = 'active', $false = '')
{
	if (!is_array($path))
	{
		return in_array($path, $activePath) ? $true : '';
	}
	else
	{
		foreach ($path as $_path)
		{
			if (in_array($_path, $activePath))
			{
				return $true;
			}
		}
	}

	return $false;
}

/*
|--------------------------------------------------------------------------
| Get carbon object from date string
|--------------------------------------------------------------------------
*/
function carbon($date = '')
{
	return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date);
}

/*
|--------------------------------------------------------------------------
| Getting General Data From Database
|--------------------------------------------------------------------------
*/
if (! function_exists('getUserName')) {
	function getUserName($id = ""){
		$userData = \DB::table('users')->where('deleted_at', null)->where('id', '=', $id)->first();
		return @$userData->name;
	}
}
