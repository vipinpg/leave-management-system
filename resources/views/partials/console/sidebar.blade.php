<div class="col-lg-3 panel-menu panel-menu-right">
    <div class="pad-top">
    </div>
    <ul>
        @if(Core::user()->isAdmin())
        <li class="title">Users</li>
        @if(Core::user()->isRoot())
            <li class="{{ in_array('console.users', $activePath) ? 'active' : '' }}"><a href="/console/users">All Users</a></li>
            <li class="{{ in_array('console.users', $activePath) ? 'active' : '' }}"><a href="/console/users/admins">Admins</a></li>
        @endif
            <li class="{{ in_array('console.users', $activePath) ? 'active' : '' }}"><a href="/console/users/employees">Employees</a></li>
        </br>
        @endif
        <li class="title">Leaves</li>
        <li class="{{ in_array('console.leaves', $activePath) ? 'active' : '' }}"><a href="/console/leaves">Leaves</a></li>
        </br>
        <li class="title">Profile</li>
        <li class="{{ in_array('console.users.changepassword', $activePath) ? 'active' : '' }}"><a href="/console/users/change-password">Change Password</a></li>
        <li><a href="/console/logout">Logout</a></li>
    </ul>
</div>