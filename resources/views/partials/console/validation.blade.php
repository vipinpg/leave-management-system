@if(isset($errors) && count($errors))
    <div class="alert alert-danger">
        <strong>ERROR</strong>
        <br/><br/>Please correct the following errors:
        <br/>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
