<nav class="navbar navbar-inverse">
<div class="container-fluid">
	<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>                        
	</button>
	<a class="navbar-brand" href="/console">Leave Management System</a>
	</div>
	<div class="collapse navbar-collapse" id="myNavbar">
	<ul class="nav navbar-nav">
		<li><a href="/console">Home</a></li>
		@if(Core::user()->isRoot())
            <li><a href="/console/users">All Users</a></li>
            <li><a href="/console/users/admins">Admins</a></li>
        @endif
        @if(Core::user()->isAdmin())
            <li><a href="/console/users/employees">Employees</a></li>
        @endif
		<li><a href="/console/leaves">Leaves</a></li>
		<li><a href="/console/users/change-password">Change Password</a></li>
		<li><a href="/console/logout">Logout</a></li>
	</ul>
	<ul class="nav navbar-nav navbar-right">
		<li><a href="#">Welcome {{ Core::user()->name() }}</a></li>
	</ul>
	</div>
</div>
</nav>