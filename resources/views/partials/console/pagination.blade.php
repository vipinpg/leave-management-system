@if (isset($records)
    && (get_class($records) == 'Illuminate\Pagination\LengthAwarePaginator')
    && !empty($records)
    && ($records->total() > $records->perPage()))
    <div class="text-right">
		<?php echo $records->appends(Input::except('page'))->links(); ?>
    </div>
@endif
