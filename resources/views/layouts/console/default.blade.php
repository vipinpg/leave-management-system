<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Title -->
        <title>Leave Management System</title>

        <!-- CSS -->
        <link rel="stylesheet" href="/css/custom.css" />
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/css/jquery-ui.css" />

        <!-- JS -->
        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <script src="/js/jquery-ui.js"></script>
        <script src="/js/bootstrap.min.js"></script>


  <script>
  $( function() {
    $( "#datepicker-input-1" ).datepicker({
        minDate: 0,
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true
    });
    
    $( "#datepicker-input-1" ).change(function() {
        var from = $( "#datepicker-input-1" ).val().split("-");
        var moded = new Date(from[2], from[1] - 1, from[0]);
        moded.setDate(moded.getDate() + 1);
        $( "#datepicker-input-2" ).datepicker({
            minDate: moded,
            dateFormat: "dd-mm-yy",
            changeMonth: true,
            changeYear: true
        });
    });
  });

  
  </script>

    </head>
    <body>
        @if(@$pageIdentifier == 'login')

            @yield('content')

        @else

           <!-- <div class="mainnav-lg mainnav-fixed navbar-fixed">-->
                <header id="navbar" class="header">
                    @include('partials.console.navbar')
                </header>

                <div class="page container">
                    <div class="row">
                        <div class="page-header {{ @$pageTitleAlignment }}">
                            <div>
                                <span class="title">
                                    {{ $pageTitle }}
                                </span>
                                @if ($pageSubTitle)
                                    — <span class="sub-title">{{ $pageSubTitle }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row page-content">
                        @yield('content')
                    </div>
                </div>

            <!--</div> -->
       
        @endif
    </body>
</html>