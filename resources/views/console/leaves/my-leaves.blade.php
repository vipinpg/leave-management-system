@extends('layouts.console.default')
@section('content')
	<div class="row panel-with-menu">
		<div class="col-lg-9">
			<div class="panel">
				<div class="panel-body no-padding">
                    <!-- sub -->
                    <div class="clearfix panel-buttons">
                        <a href="/console/leaves/add" class="btn btn-primary pull-right"><i class="ti-plus"></i> Apply Leave</a>
					</div>
					@include('flash::message')
					@if ($leaves->isEmpty())
						<div class="empty">
							<i class="ti-layers-alt"></i>
							You haven't created any leave request yet.<br/>
							Click on the 'Apply Leave' button on the top to create a new entry.
							</div>
						<br/>
					@else
					<div class="white-container" style="padding-top: 0;">
						<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="min-width"></th>
									<th style="width: 25%;" class="text-dark text-bold text-uppercase">From date</th>
									<th style="width: 25%;" class="text-dark text-bold text-uppercase">To date</th>
									<th style="width: 30%;" class="text-dark text-bold text-uppercase">Backup User</th>
									<th class="text-dark text-bold text-uppercase">Reason</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($leaves as $leave)
								<tr>
									<td><i class="ti-layers-alt icon-2x"></i></td>
									<td>
										<div class="text-dark">{{ date('j F, Y', strtotime($leave->fromDate)) }}</div>
									</td>
									<td>
										<div class="text-dark">{{ date('j F, Y', strtotime($leave->toDate)) }}</div>
									</td>
									<td>
										<div class="text-dark">{{ getUserName($leave->backup_user_id) }}</div>
									</td>
									<td>
										<div class="text-dark">{{ $leave->reason }}</div>
									</td>
		
								</tr>
							@endforeach
							</tbody>
						</table>
						@include('partials.console.pagination', ['records' => $leaves])
					</div>
					@endif
                    <!-- sub -->
				</div>
			</div>
        </div>
        @include('partials.console.sidebar')
	</div>
@stop