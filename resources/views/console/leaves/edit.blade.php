@extends('layouts.console.default')
@section('content')

	<div class="row panel-with-menu">
		<div class="col-lg-9">
			<div class="panel">
				<div class="panel-body no-padding">
                    <!-- sub -->
                    <div class="clearfix panel-buttons">
                        <a href="/console/leaves" class="btn btn-primary pull-right"><i class="ti-arrow-circle-left"></i> Go back</a>
                    </div>
                    @include('flash::message')

                    @include('partials.console.validation')

                    {!! Form::open([
                        'url' => '/console/leaves/' . (isset($leave) ? $leave->id . '/edit' : 'add'),
                        'method' => 'POST',
                        'class' => 'form-horizontal',
                        'files'     => true
                    ]) !!}

                    <div class="grey-container left-border">
                        <div class="row">
                            <div class="col-lg-6"> 
                                <div class="form-group {{ !$errors->has('from_date') ?: 'has-error' }}">
                                    <label for="from_date">From Date</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                        {{ Form::text('from_date', isset($leave) ? $leave->fromDate->format('d-m-Y') : '', [
                                            'class' => 'form-control datepicker-input',
                                            'id' => 'datepicker-input-1',
                                            'placeholder' => 'DD-MM-YYYY',
                                            'readonly' => 'readonly',
                                        ]) }}
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6"> 
                                <div class="form-group {{ !$errors->has('to_date') ?: 'has-error' }}">
                                    <label for="to_date">To Date</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                        {{ Form::text('to_date', isset($leave) ? $leave->toDate->format('d-m-Y') : '', [
                                            'class' => 'form-control datepicker-input',
                                            'id' => 'datepicker-input-2',
                                            'placeholder' => 'DD-MM-YYYY',
                                            'readonly' => 'readonly',
                                        ]) }}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">                        
                                <div class="form-group">
                                    <label for="backup_user_id">Backup User</label>
                                    <select name="backup_user_id" id="category-select" class="form-control">
                                        <option value="">Select employee</option> 
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}" {{ isset($leave) && ($leave->user_id == $user->id) ? 'selected="selected"' : '' }}>
                                                {{ $user->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">                        
                                <div class="form-group">
                                    <label for="reason">Reason for leave</label>
                                    {{ Form::textarea('reason', isset($leave) ? $leave->reason : '', [
                                        'class' => 'form-control',
                                        'id'    => 'content-editor-null',
                                        'placeholder' => 'Reason for the leave',
                                    ]) }}
                                </div>
                            </div>
                        </div>
                    </div>

                     


                    <div class="white-container text-center">
                        <button class="btn btn-primary">{{ isset($leave) ? 'Update' : 'Apply' }} Leave</button>
                    </div>
                    {!! Form::close() !!}
                    <!-- sub -->
				</div>
			</div>
        </div>
        @include('partials.console.sidebar')
	</div>

@stop