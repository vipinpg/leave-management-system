@extends('layouts.console.default')
@section('content')
	<div class="row panel-with-menu">
		<div class="col-lg-9">
			<div class="panel">
				<div class="panel-body no-padding">
                    <!-- sub -->
					<div class="empty">
						<i class="ti-layers-alt"></i>
						Welcome to Leave Management System.<br/>
						Use the options available on the top navigation bar or the right side bar.
					</div>
                    <!-- sub -->
				</div>
			</div>
        </div>
        @include('partials.console.sidebar')
	</div>
@stop