@extends('layouts.console.default')
@section('content')

	<div class="row panel-with-menu">
		<div class="col-lg-9">
			<div class="panel">
				<div class="panel-body no-padding">
                    <!-- sub -->
                    <div class="clearfix panel-buttons">
                        <a href="/console/users" class="btn btn-primary pull-right"><i class="ti-arrow-circle-left"></i> Go back</a>
                    </div>
                    @include('flash::message')

                    @include('partials.console.validation')

                    {!! Form::open([
                        'url' => '/console/users/' . (isset($user) ? $user->id . '/edit' : 'add'),
                        'method' => 'POST',
                        'class' => 'form-horizontal',
                        'files'     => true
                    ]) !!}

                    <div class="grey-container left-border">
                        <div class="row">
                            <div class="col-lg-12"> 
                                <div class="form-group {{ !$errors->has('name') ?: 'has-error' }}">
                                    <label for="name">Name</label>
                                    {{ Form::text('name', isset($user) ? $user->name : '', [
                                        'class' => 'form-control',
                                        'placeholder' => 'Enter name',
                                        'autofocus' => 'autofocus'
                                    ]) }}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12"> 
                                <div class="form-group {{ !$errors->has('email') ?: 'has-error' }}">
                                    <label for="email">Email/User ID</label>

                                        @if(isset($user))
                                            {{ Form::email('email', isset($user) ? $user->email : '', [
                                                'class'         => 'form-control',
                                                'disabled'      => 'disabled',
                                            ]) }}                                   
                                        @else
                                            {{ Form::email('email', isset($user) ? $user->email : '', [
                                                'class' => 'form-control',
                                                'placeholder' => 'Enter email address',
                                                'autofocus' => 'autofocus'
                                            ]) }}  
                                        @endif



                                </div>
                            </div>
                        </div>
                        @if(!isset($user))
                        <div class="row">
                            <div class="col-lg-12">                        
                                <div class="form-group {{ !$errors->has('password') ?: 'has-error' }}">
                                    <label for="password">Password</label>
                                    {{ Form::input('password', 'password', isset($user) ? $user->password : '', [
                                            'class' => 'form-control',
                                            'placeholder' => 'Enter password',
                                            'autofocus' => 'autofocus'
                                    ]) }} 
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-lg-12">                        
                                <div class="form-group {{ !$errors->has('is_admin') ?: 'has-error' }}">
                                    <label for="is_admin">User Type</label>
                                        <select name="is_admin" class="form-control">
                                            <option value="">Select user type</option>
                                            <option value="1" <?php if(isset($user) && $user->isAdmin == 1) { ?> selected="selected" <?php } ?>>Admin</option>
                                            <option value="0" <?php if(isset($user) && $user->isAdmin == 0) { ?> selected="selected" <?php } ?>>Employee</option>
                                        </select>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="white-container text-center">
                        <button class="btn btn-primary">{{ isset($user) ? 'Update' : 'Add' }} User</button>
                    </div>
                    {!! Form::close() !!}
                    <!-- sub -->
				</div>
			</div>
        </div>
        @include('partials.console.sidebar')
	</div>

@stop