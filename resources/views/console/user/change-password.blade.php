@extends('layouts.console.default')

@section('content')

	<div class="row panel-with-menu">
		<div class="col-lg-9">
			<div class="panel">
				<div class="panel-body no-padding">
                    <!-- sub -->
                    <div class="clearfix panel-buttons">
                        <a href="/console" class="btn btn-primary pull-right"><i class="ti-arrow-circle-left"></i> Go back</a>
                    </div>
                    @include('flash::message')
                    @include('partials.console.validation')

                    {!! Form::open([
                        'method' 	=> 'POST',
                        'class' 	=> 'form-horizontal'
                    ]) !!}
                    <div class="grey-container left-border">
                        <div class="row">
                            <div class="col-lg-4">
                                <!-- Title -->
                                <div class="form-group {{ !$errors->has('current_password') ? '' : 'has-error' }}">
                                    {{ Form::label('Current Password') }}
                                    {{ Form::password('current_password',[
                                            'class'         => 'form-control',
                                            'placeholder'   => 'Current password',
                                            'spellcheck'    => 'false'
                                        ])
                                    }}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!-- First name -->
                                <div class="form-group {{ !$errors->has('new_password') ? '' : 'has-error' }}">
                                    {{ Form::label('New Password') }}
                                    {{ Form::password('new_password',[
                                            'class'         => 'form-control',
                                            'placeholder'   => 'New password',
                                            'spellcheck'    => 'false'
                                        ])
                                    }}
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!-- Middle name -->
                                <div class="form-group {{ !$errors->has('new_password_confirmation') ? '' : 'has-error' }}">
                                    {{ Form::label('Confirm Password') }}
                                    {{ Form::password('new_password_confirmation',[
                                            'class'         => 'form-control',
                                            'placeholder'   => 'Confirm new password',
                                            'spellcheck'    => 'false'
                                        ])
                                    }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="white-container text-center">
                        <button class="btn btn-primary">Update Password</button>
                    </div>
                    {!! Form::close() !!}
                    <!-- sub -->
				</div>
			</div>
        </div>
        @include('partials.console.sidebar')
	</div>

@stop
