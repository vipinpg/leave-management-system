@extends('layouts.console.default')
@section('content')
	<div class="row panel-with-menu">
		<div class="col-lg-9">
			<div class="panel">
				<div class="panel-body no-padding">
					@if(Core::user()->isRoot())
						<!-- sub -->
						<div class="clearfix panel-buttons">
							<a href="/console/users/add" class="btn btn-primary pull-right"><i class="ti-plus"></i> Add User</a>
						</div>
					@endif
					@include('flash::message')
					@if ($users->isEmpty())
						<div class="empty">
							<i class="ti-layers-alt"></i>
							You haven't created any user yet.<br/>
							Start creating by clicking the button "Add User".
							</div>
						<br/>
					@else
					<div class="white-container" style="padding-top: 0;">
						<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-hover">
							<thead>
								<tr>
									<th style="width: 30%;" class="text-dark text-bold text-uppercase">Name</th>
									<th style="width: 30%;" class="text-dark text-bold text-uppercase">Email</th>
									<th style="width: 20%;" class="text-dark text-bold text-uppercase">Type</th>
									@if(Core::user()->isRoot())
									<th class="min-width"></th>
									@endif
								</tr>
							</thead>
							<tbody>
							@foreach ($users as $user)
								<tr>
									<td>
										<div class="text-dark">{{ $user->name }}</div>
									</td>
									<td>
										<div class="text-dark">{{ $user->email }}</div>
									</td>
									<td>
										<div class="text-dark">@if($user->isAdmin) Admin @else Employee @endif</div>
									</td>
									@if(Core::user()->isRoot())
									<td class="text-right text-nowrap">
										<a href="/console/users/{{ $user->id }}/edit" class="btn btn-primary">
											Edit
										</a>
										<a href="/console/users/{{ $user->id }}/delete" class="btn btn-dark" onclick="return confirm('Are you sure you want to delete this entry?');">
											Delete
										</a>
									</td>
									@endif
								</tr>
							@endforeach
							</tbody>
						</table>
						@include('partials.console.pagination', ['records' => $users])
					</div>
					@endif
                    <!-- sub -->
				</div>
			</div>
        </div>
        @include('partials.console.sidebar')
	</div>
@stop