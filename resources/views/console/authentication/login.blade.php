@extends('layouts.console.default')

@section('content')

    <div class="container login-page">

        
        <div class="row">
            <div class="col-xs-4"></div>
            <div class="col-xs-4">
                @include('flash::message')
                <div class="login-form">
                    <div class="title-container">
                        <div class="icon"><i class="ti-lock"></i></div>
                        <div class="title text-uppercase">Leave Managemnt System</div>
                        <p> Login to manage your leaves and requests.</p>
                    </div>
                    <div class="form-container">
                        {!! Form::open(['url' => '/console/login', 'method' => 'POST']) !!}
                        <div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="ti-user"></i></span>
                                    {!! Form::text('username', Request::input('username'), [
                                        'id'                => 'username',
                                        'class'             => 'form-control',
                                        'placeholder'       => 'Username / Email address',
                                        'autofocus'         => 'autofocus',
                                        'autocorrect'       => 'false',
                                        'autocapitalize'    => 'false',
                                        'spellcheck'        => 'false',
                                        'autocomplete'      => 'off'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="ti-lock"></i></span>
                                    {!! Form::password('password', [
                                        'id'            => 'password',
                                        'class'         => 'form-control',
                                        'placeholder'   => 'Password',
                                    ]) !!}
                                </div>
                            </div>
                            <br/>
                        </div>
                        <div>
                            <button class="btn btn-dark btn-block" type="submit">SIGN IN</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-4"></div>
            
        </div>

    </div>

@stop